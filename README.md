# luadev

Docker image that provides facilities for Lua and LuaRocks development

[View on Docker Hub](https://hub.docker.com/r/jessiehildebrandt/luadev)

[![Build Status Badge](https://gitlab.com/jessieh/luarocks-docker/badges/main/pipeline.svg)](https://gitlab.com/jessieh/luarocks-docker/-/jobs)

## Supported Tags

 - luajit
 - lua5.1
 - lua5.2
 - lua5.3
 - lua5.4

All tags are currently built on Alpine Linux.

[View tags on Docker Hub](https://hub.docker.com/repository/docker/jessiehildebrandt/luadev/tags)

## Usage

### CI/CD Pipelines

```yaml
# .gitlab-ci.yml
image: jessiehildebrandt/luadev:luajit
test:
    script:
        - luarocks build
        - luarocks test
```

### Ad Hoc / CLI

Mount your Lua project at `/workspace` and run commands as needed:

```
> docker run -it --rm --mount type=bind,source=$(pwd),target=/workspace jessiehildebrandt/luadev:luajit

🐳 luadev:luajit f3ec8ae82127 /workspace
$ luarocks build

myproject 1.0-0 depends on lua >= 5.1 (5.1-1 provided by VM)
No existing manifest. Attempting to rebuild...
myproject 1.0-0 is now installed in /usr/local (license: GPLv3)

🐳 luadev:luajit f3ec8ae82127 /workspace
$ luarocks test

myproject 1.0-0 depends on busted 2.0.0-1 (2.0.0-1 installed)
myproject 1.0-0 depends on luacov 0.15.0-1 (0.15.0-1 installed)
●●●●●●●●●●
10 successes / 0 failures / 0 errors / 0 pending : 0.07001 seconds
```

You can also supply a command for one-off execution:

```
> docker run --rm --mount type=bind,source=(pwd),target=/workspace jessiehildebrandt/luadev:luajit 'luarocks make'

myproject 1.0-0 depends on lua >= 5.1 (5.1-1 provided by VM)
mkdir -p "build"
./bin/fennel --compile src/myproject.fnl > build/myproject.lua
No existing manifest. Attempting to rebuild...
myproject 1.0-0 is now installed in /usr/local (license: GPLv3)
```
